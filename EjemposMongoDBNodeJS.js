// https://mongodb.github.io/node-mongodb-native/api-generated/collection.html
var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    //BSON = require('mongodb').pure().BSON,
    assert = require('assert');

MongoClient.connect('mongodb://admin123:admin123@ds253567.mlab.com:53567/raleman', (err,client) => {
try {
  if(err)
  {
    //return console.log("No conecto" + err);
    console.log("No conecto")
  }
  else {
      console.log("CONEXION: OK");
      var db = client.db('raleman');
 }
} catch(err) {
        console.log(err.message)
}


/******      INSERT   ****
// Fetch a collection to insert document into
  var collection = db.collection("simple_document_insert_with_function_safe");
  // Insert a single document
  collection.insert([{hello:'world_safe1'}
    , {hello:'world_safe2'}], {w:1}, function(err, result) {
    assert.equal(null, err);


    // Fetch the document
    collection.findOne({hello:'world_safe2'}, function(err, item) {
      assert.equal(null, err);
      assert.equal('world_safe2', item.hello);
      //assert.ok("function() {}", item.code);
      //db.close();
    })
  });
// */

/****   CREATE UNIQUE  INDEX       *****
// Create a collection
    var collection = db.collection('keepGoingExample');

    // Add an unique index to title to force errors in the batch insert
    collection.ensureIndex({title:1}, {unique:true}, function(err, indexName) {

      // Insert some intial data into the collection
      collection.insert([{name:"Jim"}
        , {name:"Sarah", title:"Princess"}, {w:1}], function(err, result) {

        // Force keep going flag, ignoring unique index issue
        collection.insert([{name:"Jim"}
          , {name:"Sarah", title:"Princess"}
          , {name:'Gump', title:"Gump"}
          , {w:1, keepGoing:true}], function(err, result) {

          // Count the number of documents left (should not include the duplicates)
          collection.count(function(err, count) {
            assert.equal(3, count);
          })
        });
      });
    });
*/

/********  REMOVE ALL  ++++++++
// Fetch a collection to insert document into
  db.collection("remove_all_documents_no_safe", function(err, collection) {

    // Insert a bunch of documents
    collection.insert([{a:1}, {b:2}, {w:1}], function(err, result) {
      assert.equal(null, err);

      // Remove all the document
      collection.remove();

      // Fetch all results
      collection.find().toArray(function(err, items) {
        //assert.equal(null, err);
        //assert.equal(0, items.length);
        console.log(items.length)
        //db.close();
      });
    });
  })
*/

/******   REMOVE EQUALS    +++++
db.collection("remove_subset_of_documents_safe", function(err, collection) {

    // Insert a bunch of documents
    collection.insert([{a:1}, {b:2}], {w:1}, function(err, result) {
      assert.equal(null, err);

      // Remove all the document
      collection.remove({a:1}, {w:1}, function(err, numberOfRemovedDocs) {
        assert.equal(null, err);
        assert.equal(1, numberOfRemovedDocs);
        //db.close();
      });
    });
  })
*/

/*++++++  SAVE   *****
var collection = db.collection("save_a_simple_document");
  // Save a document with no safe option
  collection.save({hello:'world'});

  // Wait for a second
  setTimeout(function() {

    // Find the saved document
    collection.findOne({hello:'world'}, function(err, item) {
      assert.equal(null, err);
      assert.equal('world', item.hello);
      //db.close();
      console.log(item.hello)
    });
  }, 1000);
*/

////    BUENO    ///////////////////////////*****************
// *****  SAVE UPDATE  ********
var collection = db.collection("save_a_simple_document_modify_it_and_resave_it");

  // Save a document with no safe option
  collection.save({hello:'world'}, {w: 0}, function(err, result) {

    // Find the saved document
    collection.findOne({hello:'world'}, function(err, item) {
      assert.equal(null, err);
      assert.equal('world', item.hello);

      // Update the document
      item['hello2'] = 'world2';
      item['hello'] = 'rayo';


      // Save the item with the additional field
      collection.save(item, {w: 1}, function(err, result) {

        // Find the changed document
        collection.findOne({hello:'rayo'}, function(err, item) {
          assert.equal(null, err);
          //assert.equal('world', item.hello);
          //assert.equal('world2', item.hello2);
          console.log(item.hello)

          //db.close();
        });
      });
    });
  });


/*****  UPDATE SIMPLE  ***************************
// Get a collection
  db.collection('update_a_simple_document', function(err, collection) {

    // Insert a document, then update it
    collection.insert([{a:1}, {w: 1}], function(err, doc) {

      // Update the document with an atomic operator
      collection.update({a:1}, {$set:{b:2} });

      // Wait for a second then fetch the document
      setTimeout(function() {

        // Fetch the document that we modified
        collection.findOne({a:1}, function(err, item) {
          assert.equal(null, err);
          assert.equal(1, item.a);
          assert.equal(2, item.b);
          //db.close();
        });
      }, 1000);
    })
  });
*/

// ******** BUENO
/*********  UPDATE  2
// Get a collection
  db.collection('update_a_simple_document_upsert', function(err, collection) {

    // Update the document using an upsert operation, ensuring creation if it does not exist
    collection.update({a:1}, {b:2, a:666,c:3}, {upsert:true, w: 1}, function(err, result) {
//      assert.equal(null, err);
//      assert.equal(1, result);
console.log(result.n)

      // Fetch the document that we modified and check if it got inserted correctly
      collection.findOne({a:1}, function(err, item) {
        //assert.equal(null, err);
        assert.equal(1, item.a);
        assert.equal(2, item.b);
        //db.close();
      });
    });
  });
*/


/******************  MULTIPLE UPDATE
// Get a collection
  db.collection('update_a_simple_document_multi', function(err, collection) {

    // Insert a couple of documentations
    collection.insert([{a:1, b:1}, {a:1, b:2}, {w: 1}], function(err, result) {

      // Update multiple documents using the multi option
      collection.update({a:1}, {$set:{b:0}}, {w: 1, multi:true}, function(err, numberUpdated) {
        assert.equal(null, err);
        //assert.equal(2, numberUpdated);
console.log('updates: '+numberUpdated)

        // Fetch all the documents and verify that we have changed the b value
        collection.find().toArray(function(err, items) {
          assert.equal(null, err);
          assert.equal(1, items[0].a);
          assert.equal(0, items[0].b);
          assert.equal(1, items[1].a);
          assert.equal(0, items[1].b);
console.log(items[0].a)
          //db.close();
        });
      })
    });
  });
*/

//////////////////  BUENO
/*////************************  KEY DISTINCT
// Crete the collection for the distinct example
  db.createCollection('simple_key_based_distinct', function(err, collection) {

    // Insert documents to perform distinct against
    collection.insert([{a:0, b:{c:'a'}}, {a:1, b:{c:'b'}}, {a:1, b:{c:'c'}},
      {a:2, b:{c:'a'}}, {a:3}, {a:3}, {w: 1}], function(err, ids) {

      // Peform a distinct query against the a field
      collection.distinct('a', function(err, docs) {
        assert.deepEqual([0, 1, 2, 3], docs.sort());
console.log('1er sort A:  ___ '+docs.sort());
        // Perform a distinct query against the sub-field b.c
        collection.distinct('b.c', function(err, docs) {
          assert.deepEqual(['a', 'b', 'c'], docs.sort());
console.log('2do sort B.C:  ___ '+docs.sort());
          //db.close();
        });
      });
    })
  });
*/

/*++++++++++++++++++++   DISTINCT filter query
// Crete the collection for the distinct example
  db.createCollection('simple_key_based_distinct_sub_query_filter', function(err, collection) {

    // Insert documents to perform distinct against
    collection.insert([{a:0, b:{c:'a'}}, {a:1, b:{c:'b'}}, {a:1, b:{c:'c'}},
      {a:2, b:{c:'a'}}, {a:3}, {a:3}, {a:666, c:1}], {w: 1}, function(err, ids) {

try{
      // Peform a distinct query with a filter against the documents
      collection.distinct('a', {c:1}, function(err, docs) {
        //assert.deepEqual([5], docs.sort());
console.log('sort A, {c:a}:  ___ '+docs.sort());
        //db.close();
      });
} catch(err) {
        console.log(err.message)
}

    })
  });
*/

/// COUNT  BUENO
/***************** Crete the collection for the distinct example
  db.createCollection('simple_count_example', function(err, collection) {

    // Insert documents to perform distinct against
    collection.insert([{a:1}, {a:2}, {a:3}, {a:4, b:1}, {w: 1}], function(err, ids) {

try{
      // Perform a total count command
      collection.count(function(err, count) {
        //assert.equal(4, count);
console.log(count)
        // Peform a partial account where b=1
        collection.count({b:1}, function(err, count) {
          //assert.equal(1, count);
console.log(count)
        });
      });
} catch(err){
  console.log(err.message)
}
    });
  });
*/

/**************************   find And Modify
// Create a collection we want to drop later
  db.createCollection('simple_find_and_modify_operations_', function(err, collection) {
    assert.equal(null, err);

    // Insert some test documentations
    collection.insert([{a:1}, {b:1}, {c:1}, {w:1}], function(err, result) {
      assert.equal(null, err);

      // Simple findAndModify command returning the new document
      collection.findAndModify({a:1}, [['a', 1]], {$set:{b1:1}}, {new:true}, function(err, doc) {
//        assert.equal(null, err);
//        assert.equal(1, doc.a);
//        assert.equal(1, doc.b1);

        // Simple findAndModify command returning the new document and
        // removing it at the same time
        collection.findAndModify({b:1}, [['b', 1]],
          {$set:{b:2}}, {remove:true}, function(err, doc) {

          // Verify that the document is gone
          collection.findOne({b:1}, function(err, item) {
//            assert.equal(null, err);
//            assert.equal(null, item);

            // Simple findAndModify command performing an upsert and returning the new document
            // executing the command safely
            collection.findAndModify({d:1}, [['b', 1]],
              {d:1, f:1}, {new:true, upsert:true, w:1}, function(err, doc) {
//                assert.equal(null, err);
//                assert.equal(1, doc.d);
//                assert.equal(1, doc.f);
            })
          });
        });
      });
    });
  });
*/

/*************************** BUENO  find And Remove
// Create a collection we want to drop later
  db.createCollection('simple_find_and_remove', function(err, collection) {
    assert.equal(null, err);

    // Insert some test documentations
    collection.insert([{a:1}, {b:1, d:1}, {c:1}, {w:1}], function(err, result) {
      assert.equal(null, err);

      // Simple findAndModify command returning the old document and
      // removing it at the same time
      collection.findAndRemove({b:1}, [['b', 1]], function(err, doc) {

        // Verify that the document is gone
        collection.findOne({b:1}, function(err, item) {
        });
      });
    });
  });
*/

/*
try{
  // Create a collection we want to drop later
    db.createCollection('simple_limit_skip_query', function(err, collection) {
      assert.equal(null, err);

      // Insert a bunch of documents for the testing
    //  collection.insert([{a:1, b:1}, {a:2, b:2}, {a:3, b:3}], {w:1}, function(err, result) {

        // Peform a simple find and return all the documents
        collection.find({}, {skip:2, limit:2, fields:{b:1}}).toArray(function(err, docs) {
          console.log( docs.length);
          console.log( docs[0].a);
          console.log( docs[0].b);
        });
      //  });
    });
}catch(err){
  cnsole.log(err.message)
}
*/

/*
try{
  // Some docs for insertion
    var docs = [{
        title : "this is my title", author : "bob", posted : new Date() ,
        pageViews : 5, tags : [ "fun" , "good" , "fun" ], other : { foo : 5 },
        comments : [
          { author :"joe", text : "this is cool" }, { author :"sam", text : "this is bad" }
        ]}];

    // Create a collection
    var collection = db.collection('shouldCorrectlyExecuteSimpleAggregationPipelineUsingArray');
    // Insert the docs
    //collection.insert(docs, {w: 1}, function(err, result) {

      // Execute aggregate, notice the pipeline is expressed as an Array
      collection.aggregate([
          { $project : {
            author : 1,
            tags : 1
          }},
          { $unwind : "$tags" },
          { $group : {
            _id : {tags : "$tags"},
            authors : { $addToSet : "$author" }
          }}
        ], function(err, result) {
//          assert.equal(null, err);
//          assert.equal('good', result[0]._id.tags);
//          assert.deepEqual(['bob'], result[0].authors);
//          assert.equal('fun', result[1]._id.tags);
//          assert.deepEqual(['bob'], result[1].authors);

          //db.close();
//      });
    });
}catch(err){
  console.log(err.message)
}
*/


})
