//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var bodyParser = require('body-parser');

var db= null, collection = null;
var mongodb = require('mongodb');
//const MongoClient = mongodb.MongoClient;

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    //BSON = require('mongodb').pure().BSON,
    assert = require('assert');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true }));


MongoClient.connect('mongodb://admin123:admin123@ds253567.mlab.com:53567/raleman', (err,client) => {
  try {
    if(err)
    {
      return console.log("No conecto" + err);
    }
    else {
        console.log("CONEXION BD: OK");
        db = client.db('raleman');
   }
  } catch(err) {
          console.log(err.message)
  }
});

var requestjson= require('request-json');

var urlMlabRaiz ="https://api.mlab.com/api/1/databases/raleman/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

var urlMovimientosMlab ="https://api.mlab.com/api/1/databases/raleman/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlMovimientosMlab);

var path = require('path');

var bodyparser = require('body-parser');
app.use(bodyparser.json())
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
  next();
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
var movimientoJSON = require('./movimientosv2.json');


app.get ('/', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/Movimientos/v2/:index/:otroParam', function(req, res) {
 console.log(req.params.index-1);
 console.log(req.params.otroParam);
 //res.send('Hola Mundo nodejs');
 res.send(movimientoJSON[req.params.index-1]);
});

app.post('/Movimientos', function(req,res){
  clienteMLab.post ('',req.body, function(err, resM, body){
    if(err){
      console.log(body);
    } else{
      res.send(body);
    }
  })
});

//CONSULTA DE USUARIOS
/*app.post('/apitechu/v5/login', function(req,res){
  var bcrypt = require('bcrypt');

  var email = req.headers.email;
  var password = req.headers.password;
  var query = 'q={"email":"' + email + '","password":"' + password + '"}';
  //console.log(query);

  clienteMLabRaiz = requestjson.createClient(urlMlabRaiz + "/Usuarios?" + apiKey + "&" + query);

//console.log(urlMlabRaiz + "/Usuarios?" + apiKey + "&" + query);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length >= 1){ // Login OK
        res.status(200).send("ok");
        console.log("Usuario encontrado.");
      }else{
        res.status(400).send("npok");
        console.log("Usuario NOP encontrado resM. " + resM);
        console.log("Usuario NOP encontrado body. " + body);
      }
    } else{
      res.status(400).send("nok");
      console.log("Usuario NOK encontrado resM. " + resM);
      console.log("Usuario NOK encontrado body. " + body);
    }
  })
});*/

//Desactiva usuario
app.post('/desactiva', function(req, res) {
      try{
        var email = req.headers.email;
        collection = db.collection("Usuarios");
        collection.findOne({email:req.headers.email}, function(err, item) {
        if (item == null ) {
          console.log("Usuario no encontrado.");
          return res.status(400).send("noUSER");
        }

        item['estatus'] = false;
        collection.save(item, {w: 1}, function(err, result) {
        console.log("Desactiva 'SAVE'... " + result);
        /// aqui
        return res.status(200).send("okSAVE");
        //return res.status(400).send("NokSAVE");
          });
        });
      } catch(err){
          console.log("Error en login: " + err);
      }
  }); // cierra POST desactiva

//NUEVO LOGIN
app.post('/login', function(req, res) {
      try{
        //console.log("Entro nuevo login.");
        var bcrypt = require('bcrypt');
        var email = req.headers.email;
        var password = req.headers.password;
        //var query = 'q={"email":"' + email + '","password":"' + password + '"}';

        collection = db.collection("Usuarios");
        collection.findOne({email:req.headers.email}, function(err, item) {
          if(err) {
            console.log("Sin acceso a BD.");
            res.status(400).send("noBD");
          }
          else{
            if (item == null ) {
              console.log("Usuario no encontrado.");
              return res.status(400).send("noUSER");
            }
            console.log("Usuario existente");
            //console.log("pwd: " + item.password + " nombre: " + item.nombre + " estatus: " + item.estatus);

            bcrypt.compare(password, item.password, function(err, resp) {
              console.log("bcrypt.compare ( " + resp + " )");
              if(resp){
                console.log("Estatus (" + item.estatus + ") del usuario ( " + item.email + " )");
                if(item.estatus){
                  console.log("LOGEO 'OK'");
                  res.status(200).send("ok");
                }else{
                  console.log("LOGEO 'NOK'");
                  res.status(400).send("noSTATUS");
                }
              }else{
                console.log("LOGEO 'noPWD'");
                res.status(400).send("noPWD");
              }
            });  // cierra bcrypt.compare
          }
          })
      } catch(err){
          console.log("Error en login: " + err);
      }
}); // cierra POST login

// Registro de usuario nuevo
app.post('/registro', function(req, res) {
    var bcrypt = require('bcrypt');

      try{
        var email = req.headers.email;
        var password = "";
        var nombre = req.headers.nombre;

        bcrypt.genSalt(10, function(err, salt) {
          bcrypt.hash(req.headers.password, salt, function(err, crypted) {
            console.log('Registro Password Crypted: ' + crypted);
            collection = db.collection("Usuarios");
            collection.insert([{"id" : 999,"email" : email,"password" : crypted, "nombre" : nombre ,"estatus" : true}], function(err, doc) {
              console.log("Resultado Insert Registro: " + doc);
              collection.findOne({email:req.headers.email}, function(err2, item) {
                if(err2) {
                  console.log("Sin acceso a BD.");
                  res.status(400).send("Registro .. noBD");
                }
                else {
                  if (item == null ) {
                    console.log("Registro fallidos.");
                    return res.status(400).send("Registro fallidos");
                  } else {
                    console.log("Usuario " + item.nombre + " (" + item._id + ") creado correctamente.");
                    return res.status(200).send("okINSERT");
                  }
                }
                });
            });
          });
        });
      } catch(err){
          console.log("Error en login: " + err);
      }
}); // cierra POST registro

// Prueba de hash con Bcrypt
app.get('/hash', function(req, res) {
  var bcrypt = require('bcrypt');

  var stored_hash = '$2b$10$XbzuGAJLCzsLBunYlgFruut9OEuu4P17dX5xicBoxqdUHwJpLz8Oq';
  bcrypt.compare('1234', stored_hash, function(err, res) {
    console.log('compared true: ' + res);
  });

/*
  bcrypt.genSalt(10, function(err, salt) {
//    console.log('salt: ' + salt);
    bcrypt.hash('1234', salt, function(err, crypted) {
      console.log('crypted: ' + crypted);
      res.send(crypted);
      bcrypt.compare('$2b$10$XbzuGAJLCzsLBunYlgFruut9OEuu4P17dX5xicBoxqdUHwJpLz8Oq', crypted, function(err, res) {
        console.log('compared true: ' + res);
      });
  /*    bcrypt.compare('bacon', crypted, function(err, res) {
        console.log('compared false: ' + res);
        console.log('compared false cb end: ' + (Date.now() - start) + 'ms');
      });
    });
  })*/
});

//CONSULTA DE CLIENTES
app.get('/Clientes', function(req, res) {
console.log("Consulta de todos los clientes.");

  db.collection('Clientes').find({}).toArray(function(err, result) {
    if(err) return console.log(err)

    res.send(result)
  })
});

//CONSULTA DE CLIENTES
app.get('/Clientes_Contratos', function(req, res) {
console.log("Consulta de todos los clientes.Contratos.");

  db.collection('Clientes').find({},{projection:{Contratos:1}}).toArray(function(err, result) {
    if(err) return console.log(err)

    res.send(result)
  })
});

//:::::::  CONSULTA CLIENTES  :::::::::::
app.get('/Clientes/:idCli', function(req, res) {
var id = req.params.idCli;
console.log("Buscando Cliente id : " + id);

   db.collection('Clientes').find({"_id" : new mongodb.ObjectID(id) }).toArray(function(err, result) {
    if(err) return console.log(err)
   res.send(result)
   })
});

//:::::::  ELIMINA CLIENTE   :::::::::::
app.get('/eliminarcliente/:id', function(req,res){
  console.log("Eliminando Cliente id : " + req.params.id);
   db.collection('Clientes').deleteOne({ "_id": new mongodb.ObjectID(req.params.id) }, function(err, result) {
	 if(err) return console.log(err)

	console.log('Id ' + req.params.id + ' eliminado de BD');
   })
});
//INSERT DE CLIENTE NUEVO
app.post('/insertarcliente', function(req,res)
{
  console.log('Insertando cliente nuevo.')
  db.collection('Clientes').save(req.body, function(err, result) {
    if(err) return console.log(err)

      console.log('guardado en BD')
  })
});
