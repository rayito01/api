//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var bodyParser = require('body-parser');

var db;
var mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true }));


MongoClient.connect('mongodb://admin123:admin123@ds253567.mlab.com:53567/raleman', (err,client) => {
  try {
    if(err)
    {
      return console.log("No conecto" + err);
    }
    else {
        console.log("CONEXION BD: OK");
        var db = client.db('raleman');
   }
  } catch(err) {
          console.log(err.message)
  }
});


var requestjson= require('request-json');

var urlMlabRaiz ="https://api.mlab.com/api/1/databases/raleman/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

var urlMovimientosMlab ="https://api.mlab.com/api/1/databases/raleman/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlMovimientosMlab);

var path = require('path');

var bodyparser = require('body-parser');
app.use(bodyparser.json())
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
  next();
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
var movimientoJSON = require('./movimientosv2.json');


app.get ('/', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get ('/Clientes/v4', function(req, res) {
  res.send('Aqui estan los clientes devueltos nuevos.');
});

/*app.get ('/Clientes/:idcliente', function(req, res) {
  res.send('Aqui tiene al cliente numero:'+ req.params.idcliente);
});*/

app.get ('/Movimientos/v1', function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get ('/Movimientos/v2', function(req, res) {
  res.send(movimientoJSON);
});

app.get('/Movimientos/v2/:index/:otroParam', function(req, res) {
 console.log(req.params.index-1);
 console.log(req.params.otroParam);
 //res.send('Hola Mundo nodejs');
 res.send(movimientoJSON[req.params.index-1]);
});

app.get('/MovimientosQ/v2', function(req, res) {
 console.log(req.query);
 //res.send('Hola Mundo nodejs');
 res.send('Recibido');
});

app.post('/', function(req,res){
  res.send('Hemos recibido una peticion POST');
});

app.post('/Movimientos/v2', function(req,res){
  var nuevo = req.body
  nuevo.id = movimientoJSON.length + 1;
  movimientoJSON.push(nuevo)
  res.send('Movimiento dado de alta');
});

app.get('/Movimientos/v3', function(req, res) {
  clienteMLab.get ('',function(err, resM, body){
    if(err){
      console.log(body);
    } else{
      res.send(body);
    }
  })
});

app.post('/Movimientos', function(req,res){
  clienteMLab.post ('',req.body, function(err, resM, body){
    if(err){
      console.log(body);
    } else{
      res.send(body);
    }
  })
});

//CONSULTA DE USUARIOS
app.post('/apitechu/v5/login', function(req,res){
  var bcrypt = require('bcrypt');

  var email = req.headers.email;
  var password = req.headers.password;
  var query = 'q={"email":"' + email + '","password":"' + password + '"}';
  //console.log(query);

  clienteMLabRaiz = requestjson.createClient(urlMlabRaiz + "/Usuarios?" + apiKey + "&" + query);

//console.log(urlMlabRaiz + "/Usuarios?" + apiKey + "&" + query);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length >= 1){ // Login OK
        res.status(200).send("ok");
        console.log("Usuario encontrado.");
      }else{
        res.status(400).send("npok");
        console.log("Usuario NOP encontrado resM. " + resM);
        console.log("Usuario NOP encontrado body. " + body);
      }
    } else{
      res.status(400).send("nok");
      console.log("Usuario NOK encontrado resM. " + resM);
      console.log("Usuario NOK encontrado body. " + body);
    }
  })
});

app.get('/hash', function(req, res) {
  var bcrypt = require('bcrypt');

  var start = Date.now();
  bcrypt.genSalt(10, function(err, salt) {
//    console.log('salt: ' + salt);
//    console.log('salt cb end: ' + (Date.now() - start) + 'ms');
    bcrypt.hash('1234', salt, function(err, crypted) {
      console.log('crypted: ' + crypted);
      res.send(crypted);
  //    console.log('crypted cb end: ' + (Date.now() - start) + 'ms');
  //    console.log('rounds used from hash:', bcrypt.getRounds(crypted));
      bcrypt.compare('1234', crypted, function(err, res) {
        console.log('compared true: ' + res);
  //      console.log('compared true cb end: ' + (Date.now() - start) + 'ms');
      });
  /*    bcrypt.compare('bacon', crypted, function(err, res) {
        console.log('compared false: ' + res);
        console.log('compared false cb end: ' + (Date.now() - start) + 'ms');
      });*/
    });
  })
  //console.log('end: ' + (Date.now() - start) + 'ms');

  //res.send('Recibido');
});

//CONSULTA DE CLIENTES
app.get('/Clientes', function(req, res) {
console.log("Consulta de todos los clientes.");

  db.collection('Clientes').find({}).toArray(function(err, result) {
    if(err) return console.log(err)

    res.send(result)
  })
});

//:::::::  CONSULTA CLIENTES  :::::::::::
app.get('/Clientes/:idCli', function(req, res) {
var id = req.params.idCli;
console.log("Buscando Cliente id : " + id);

   db.collection('Clientes').find({"_id" : new mongodb.ObjectID(id) }).toArray(function(err, result) {
    if(err) return console.log(err)
   res.send(result)
   })
});

//:::::::  ELIMINA CLIENTE   :::::::::::
app.get('/eliminarcliente/:id', function(req,res){
  console.log("Eliminando Cliente id : " + req.params.id);
   db.collection('Clientes').deleteOne({ "_id": new mongodb.ObjectID(req.params.id) }, function(err, result) {
	 if(err) return console.log(err)

	console.log('Id ' + req.params.id + ' eliminado de BD');
   })
});
//INSERT DE CUENTAS
app.post('/insertarcliente', function(req,res)
{
  console.log('Insertando cliente nuevo.')
  db.collection('Clientes').save(req.body, function(err, result) {
    if(err) return console.log(err)

      console.log('guardado en BD')
  })
});
